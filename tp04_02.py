import os
def menu():
    print('1. registrar producto')
    print('2. mostrar listado de productos')
    print('3. mostrar productos cuyo stock se encuentre el el untervalo [desde, hasta]')
    print('4. diseñe un proceso que le sume X stock de todos los productos cuyo valor actual sea menor al valor y ')
    print('5. eliminar todos los productos cuyo stock sea igual a cero')
    print('6. salir')
    opc= int(input('\ningrese opcion: '))
    return opc

def cargar_diccionario(productos):
    os.system('cls')
    respuesta='s'
    while(respuesta == 's' or respuesta == 'S'):
        clave=validar_numero('codigo: ')
        if (clave not in productos):
            descripcion = input("Ingrese Descripcion: ")
            precio = float(input("Ingrese precio: "))
            stock = int(input('Ingrese stock: '))
            productos [int(clave)] = [descripcion,precio,stock]
        else: 
            print('Ingrese un codigo no Existente')
        respuesta=input('quiere continuar cargando productos? (s / n) :')
    return productos

def mostrar_productos(diccionario):
    print('Listado de Alumnos')
    for (clave, valor) in diccionario.items():
        print(f'clave: {clave} tiene: {valor[0]} con precio: {valor[1]} y Stock: {valor[2]}')

def mostrar_intervalo(productos,):
    desde = validar_numero("Ingrese DESDE: ")
    hasta = validar_numero("Ingrese HASTA: ")
    no_encontro = True
    for (clave, valor) in productos.items() :
        if valor[2] >= desde and valor[2] <= hasta:
            no_encontro=False
            print(f'clave: {clave} tiene: {valor[0]} con precio: {valor[1]} y Stock: {valor[2]}')
    if no_encontro:
        print("No se encontraron Valores en el Intervalo")

def suma_X_stock_menores_que_Y(productos):
    y = validar_numero("valor de stock a quienes agregar(incluye los valores inferiores al ingresado): ")
    x = positivo('el Stock que va agregar: ')
    for (clave, valor) in productos.items() :
        if valor[2] < y:
            productos[clave] = [valor[0],valor[1],valor[2]+x]

def eliminar_productos_sin_stock(productos):
    listaAux=[]
    for (clave, valor) in productos.items():
        if valor[2] == 0 :
            listaAux.append(clave)
    eliminar_vacios(productos,listaAux)
    listaAux.clear()

def eliminar_vacios(x, lista):
    for item in lista:
        del x[item]

def validar_numero(mensaje):
    while True:
        numero = input(f"\nIngrese {mensaje}")
        if numero.isdigit() == True:
            return int(numero)
        print("Ingrese un valor valido (nro entero positivo)")

def positivo(mensaje):
    while True:
        nro = validar_numero(mensaje)
        if nro > 0:
            return nro
        print('Ingrese un valor positivo!!!')

opc=0
productos= {}
while opc!= 6:
    os.system('cls')
    opc=menu()
    if opc==1:
       cargar_diccionario(productos)
    elif opc == 2 and len(productos)!= 0:
        mostrar_productos(productos)
    elif opc == 3 and len(productos)!= 0:
        mostrar_intervalo(productos)
    elif opc == 4 and len(productos)!= 0:
        suma_X_stock_menores_que_Y(productos)
    elif opc == 5 and len(productos)!= 0:
        eliminar_productos_sin_stock(productos)
    elif opc == 6 and len(productos)!= 0:
        print('fin programa ')
    input('Presionar tecla ...')

    