import os
from operator import itemgetter
# v=[dominio,marca,tipo,modelo,k,estado,precio_venta]

def menu():
    while True:
        os.system('cls')
        print('1. Agregar vehiculos')
        print('2. Reservar un automovil')
        print('3. Buscar automovil')
        print('4. Ordenar la lista por marca')
        print('5. ordenar la lista por precio')
        print('6. Salir')
        opcion = int(input('Ingrese opción: '))
        if opcion >=1 and opcion <=6:
            return opcion
        print('Ingrese una opcion valida !!')
        input()

def agregar_vehiculo(lista):
    v=[]
    dominio = validar_dominio()
    marca= validar_marca()
    tipo = validar_tipo()
    modelo = validar_modelo()
    k = validar_kilometraje()
    estado = 'Disponible'
    precio = validar_precio()
    precio_venta = precio*1.10
    v=[dominio,marca,tipo,modelo,k,estado,precio,round(precio_venta,2)]
    lista.append(v)

def validar_dominio():
    dominio = validar_AlfaNumerico()
    while(len(dominio)<6 or len(dominio)>9):
        dominio = input('Ingrese un dominio valido (entre 6 y 9 caracteres): ')
    return dominio
def validar_AlfaNumerico():
    while True:
        d = input('Ingrese dominio (entre 6 y 9 caracteres): ')
        if d.isalnum:
            return d
        print('No es un dominio valido')
def validar_marca():
    marca = input('Ingrese marca del vehiculo (R=Renault, F=Ford, C=Citroen): ')
    while (marca.lower() != 'r' and  marca.lower() != 'f' and marca.lower() != 'c') :
        marca = input('Ingrese una marca valida(R=Renault, F=Ford, C=Citroen): ')
    marca = nombrar_marca(marca)
    return marca
def nombrar_marca(x):
    if x == 'r':
        return "Renault"
    elif x == 'f':
        return "Ford"
    return "Citroen"
def validar_tipo():
    tipo = input('Ingrese Tipo ( U=Utilitario, A=Automóvil ): ')
    while (tipo.lower() != 'u' and  tipo.lower() != 'a') :
        tipo = input('Ingrese un Tipo valido ( U=Utilitario, A=Automóvil ): ')
    if tipo == 'u':
        return "Utilitario"
    return "Automovil"
def validar_modelo():
    modelo = int(input('Ingrese Modelo [2005, 2020]: '))
    while (modelo < 2005 or modelo > 2020):
        modelo = int( input('Ingrese Un modelo entre [2005, 2020]: '))
    return modelo
def validar_kilometraje():
    while True:
        k = input('ingrese Kilometraje: ')
        if k.isnumeric:
            if int(k) >= 0:
                return k
        print('Kilometraje no valido!!')
def validar_precio():
    p = float(input('Ingrese Precio: '))
    while( p <= 0):
         print('NO es un precio valido')
         p= float(input('Ingrese precio: '))
    return p
def mostrar_lista(lista):
    print('Lista de automoviles\n')
    for (i, item) in enumerate (lista):
        print(f"{i} - dominio: {item[0]}, marca: {item[1]}, tipo: {item[2]}, modelo: {item[3]}, kilometraje: {item[4]}, estado: {item[5]}, precio de venta: {item[6]}")

def reservar(lista):
    disponibles = []
    print(" lista disponibles")
    mostrar_disponibles(lista,disponibles)
    reservar_disponibles(lista,disponibles)
    disponibles.clear()
def reservar_disponibles(lista,disponibles):
    r = int(input('¿Que vehiculo quiere reservar? (elija su nro correspondiente): '))
    if r in disponibles:
        lista[r][5] = "Reservado"
    else:
        print(' No se puede reservar ')
def mostrar_disponibles(lista,disponibles):
    for (i,item) in enumerate(lista):
        if not(item[5] != "Disponible"):
            print(f"VEHICULO {i} - dominio: {item[0]}, marca: {item[1]}, tipo: {item[2]}, modelo: {item[3]}, kilometraje: {item[4]}, estado: {item[5]}, precio: {item[6]}, precio de venta: {item[7]}")
            disponibles.append(i)
        else:
            print(f"VEHICULO {i} - Vendido o Reservado")
   
def buscar(lista):
    aux= True
    dominio = validar_dominio()
    for (i,item) in enumerate(lista):
        if item[0] == dominio:
            aux=False
            print(f"VEHICULO {i} - dominio: {item[0]}, marca: {item[1]}, tipo: {item[2]}, modelo: {item[3]}, kilometraje: {item[4]}, estado: {item[5]}, precio de venta: {item[6]}")
    if aux :
       print('No se encontró')

def ordenar_lista(lista,opc,n):
    if opc == 1:
        lista.sort(key=itemgetter(n))
    else:
        lista.sort(key=itemgetter(n),reverse = True)
def ordenar_por_marca(lista):
    opc=-1
    while opc < 1 or opc > 2:
        opc = int(input('1- ascendente\n2- descendente\nIngrese opcion: '))    
        if opc != 1 and opc !=2:
            print('Ingrese una opcion valida!!')
    ordenar_lista(lista,opc,1)
    mostrar_lista(lista)
def ordenar_por_precio(lista):
    opc=-1
    aux=[]
    while opc < 1 or opc > 2:
        opc = int(input('1- ascendente\n2- descendente\nIngrese opcion: '))    
        if opc != 1 and opc !=2:
            print('Ingrese una opcion valida!!')
    ordenar_lista(lista,opc,6)
    mostrar_disponibles(lista,aux)

os.system('cls')
vehiculos = [   ["123papa", "Renault", "Automovil", 2011, 0, "Disponible", 110.00,110*1.1],
                ["100aaa", "Ford", "Utilitario", 2010, 0, "Reservado", 200.00,200*1.1],
                ["999nnn", "Citroen", "Utilitario", 2009, 0, "Vendido", 999.00, 999*1.1],
                ["200bbb", "Ford", "Automovil", 2020, 0, "Disponible", 350.00],
                ["222fff", "Ford", "Utilitario", 2020, 0, "Disponible", 350.00,(350*1.1)],
                ["8880bbb", "Ford", "Automovil", 2020, 0, "Disponible", 380.00,380*1.1],
                ["4040pp", "Citroen", "Automovil", 2020, 0, "Disponible", 350.00,350*1.1],
                ["kkk666", "Renault", "Automovil", 2011, 0, "Disponible", 110.00,110*1.1],
                ["mmm4l4", "Renault", "Utilitario", 2011, 0, "Disponible", 180.00,180*1.1],
                ["777veg", "Renault", "Automovil", 2011, 0, "Disponible", 140.00,140*1.1]]
opc = -1
while opc !=6:
    opc = menu()
    if opc == 1:
        agregar_vehiculo(vehiculos)
    elif opc == 2 and len(vehiculos) != 0:
        reservar(vehiculos)
    elif opc == 3 and len(vehiculos) != 0:
        buscar(vehiculos)
    elif opc == 4 and len(vehiculos) != 0:
        ordenar_por_marca(vehiculos)
    elif opc == 5 and len(vehiculos) != 0:
        ordenar_por_precio(vehiculos)
    elif opc == 6:
        print('Fin programa')
    else:
        print('La lista de vehiculos no está cargada !!')
    input("Enter para continuar...")